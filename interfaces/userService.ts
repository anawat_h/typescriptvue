export interface UserService {
    loadUsers: (page: number) => Promise<User[]>

    readonly rowCount: number
    readonly pageCount: number
    readonly currentPage: number
    readonly items: User[]
    readonly from: number
    readonly to: number
}

export interface User {
    id: number
    email: string
    first_name: string
    last_name: string
    avatar: string
}
