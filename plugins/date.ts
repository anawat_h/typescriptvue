import { Plugin } from '@nuxt/types'
import { DateTime } from 'luxon'

declare module 'vue/types/vue' {
    interface Vue {
        $date: Date
    }
}

declare module '@nuxt/types' {
    interface Context {
        $date: Date
    }
}

interface Date {
    display(date: DateTime): string
    read(date: string): DateTime
}

const initialization: Plugin = (context, inject) => {
    const date: Date = {
        display: function(date) {
            return date.toFormat('dd/MM/yyyy')
        },
        read: function(date) {
            return DateTime.fromFormat(date, 'dd/MM/yyyy')
        }
    }

    inject('date', date);
}
export default initialization
