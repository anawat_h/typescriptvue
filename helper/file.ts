/**
 * @param fileName ชื่อ file ตัวอย่าง: 'download.json'
 */
export function downloadAsJsonFile<T = any>(data: T, fileName: string){
    const jsonString = JSON.stringify(data, null, '\t')
    const blob = convertToBlob(jsonString)
    downloadFile(blob, fileName, 'application/json')
}

export function convertToBlob(data: string): Blob{
    const encoder = new TextEncoder()
    const uint8Array = encoder.encode(data)
    return new Blob([uint8Array])
}

/** จะนำ Blob มาสร้าง tag a ด้วย ObjectURL และสั่งให้ browser click ที่ tag a นั้น ก่อนที่จะทำการลบ tag a นี้ทิ้งออกไป
 * @param data ข้อมูล Blob
 * @param fileName ชื่อ file ตัวอย่าง: 'download.json'
 * @param mimeType (Optional) MIME Type ของ file ตัวอย่าง: 'application/json'
 */
export function downloadFile(data: Blob, fileName: string, mimeType?: string) {
    const createObjectURL = (window.URL && window.URL.createObjectURL) || window.webkitURL.createObjectURL
    const blobURL = createObjectURL(data)
    var anchor = document.createElement('a')
    anchor.style.display = 'none'
    anchor.href = blobURL
    anchor.setAttribute('download', fileName)
    if (mimeType)
        anchor.setAttribute('type', mimeType)

    // Safari thinks _blank anchor are pop ups. We only want to set _blank
    // target if the browser does not support the HTML5 download attribute.
    // This allows you to download files in desktop safari if pop up blocking
    // is enabled.
    if (typeof anchor.download === 'undefined') {
        anchor.setAttribute('target', '_blank')
    }

    document.body.appendChild(anchor)
    anchor.click()

    // Fixes "webkit blob resource error 1"
    setTimeout(function() {
        document.body.removeChild(anchor)
        window.URL.revokeObjectURL(blobURL)
    }, 200)
}
