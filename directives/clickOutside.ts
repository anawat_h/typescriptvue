import { DirectiveOptions } from 'vue'

interface Element extends HTMLElement{
    clickOutsideEvent: EventListener
}

const clickOutside: DirectiveOptions = {
    bind (htmlElement, binding, vnode) {
        const ele = htmlElement as Element
        ele.clickOutsideEvent = function (event) {
            // here I check that click was outside the el and his children
            if (!(ele === event.target || ele.contains(event.target as Node))) {
                // and if it did, call method provided in attribute value
                vnode.context[binding.expression](event)
            }
        }
        document.body.addEventListener('click', ele.clickOutsideEvent)
    },
    unbind (htmlElement) {
        const ele = htmlElement as Element
        document.body.removeEventListener('click', ele.clickOutsideEvent)
    }
}
export default clickOutside
