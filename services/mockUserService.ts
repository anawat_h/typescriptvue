import { reactive } from '@nuxtjs/composition-api'
import { UserService, User } from '@/interfaces/userService'

const rowCount = 100
const pageSize = 5
const pageCount = Math.ceil(rowCount / pageSize)
const state = reactive({
    rowCount: 0,
    pageCount: 0,
    currentPage: 0,
    items: [] as User[],
    from: 0,
    to: 0,
})

const users = generateData(rowCount)
function generateData(count: number): User[]{    
    const items = []
    for (let i = 0; i < count; i++){
        items.push({
            id: i,
            email: `mockdata${i}@mock.com`,
            first_name: `mock${i}`,
            last_name: `data${i}`,
            avatar: 'https://image.shutterstock.com/image-vector/gray-photo-placeholder-icon-design-260nw-1898064247.jpg',
        })
    }
    return items
}

async function loadUsers(page: number): Promise<User[]>{
    state.rowCount = rowCount
    state.pageCount = pageCount
    state.currentPage = page
    state.from = Math.min(state.currentPage * pageSize + 1, state.rowCount)
    state.to = Math.min((state.currentPage + 1) * pageSize, state.rowCount)
    state.items = users.slice(Math.max(state.from - 1, 0), Math.max(state.to, 0))

    return state.items
}

const service: UserService = {
    loadUsers,
    get rowCount(){
        return state.rowCount
    },
    get pageCount(){
        return state.pageCount
    },
    get currentPage(){
        return state.currentPage
    },
    get items(){
        return state.items
    },
    get from(){
        return state.from
    },
    get to(){
        return state.to
    }
}
export default service
