import { reactive } from '@nuxtjs/composition-api'
import { UserService, User } from '@/interfaces/userService'
import Axios from 'axios'

const endpoint = 'https://reqres.in'

interface APIResult {
    page: number,
    per_page: number,
    total: number,
    data: User[],
}

const pageSize = 5
const state = reactive({
    rowCount: 0,
    pageCount: 0,
    currentPage: 0,
    items: [] as User[],
    from: 0,
    to: 0,
})

function requestAPI(page: number): Promise<APIResult>{
    return Axios.get<APIResult>(`${endpoint}/api/users`
        , {
            params: {
                page: page,
                per_page: pageSize,
            },
        })
        .then(result => result.data)
        .catch((): APIResult => ({
            page: 0,
            per_page: 0,
            total: 0,
            data: [],
        }))
}

async function loadUsers(page: number): Promise<User[]>{
    const resp = await requestAPI(page + 1)
    state.rowCount = resp.total
    state.pageCount = Math.ceil(state.rowCount / pageSize)
    state.currentPage = resp.page - 1
    state.items = resp.data
    state.from = Math.min(state.currentPage * pageSize + 1, state.rowCount)
    state.to = Math.min((state.currentPage + 1) * pageSize, state.rowCount)

    return state.items
}

const service: UserService = {
    loadUsers,
    get rowCount(){
        return state.rowCount
    },
    get pageCount(){
        return state.pageCount
    },
    get currentPage(){
        return state.currentPage
    },
    get items(){
        return state.items
    },
    get from(){
        return state.from
    },
    get to(){
        return state.to
    }
}
export default service
