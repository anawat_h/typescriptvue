import { StoreOptions } from 'vuex'
import drawing from './modules/drawing'

export interface RootState {
}

const module: StoreOptions<RootState> = {
    state: () => ({}),
    getters: {},
    actions: {},
    mutations: {},
    modules: {
        drawing,
    }
}

export default module
