import { GetterTree, ActionTree, MutationTree, Module } from 'vuex'
import { Store, ActionContext } from 'vuex'
import { RootState } from '../index'
import { Coord } from '@/interfaces/drawing'

export interface State {
    history: Coord[][]
    index: number
}

const state = (): State => ({
    history: [],
    index: undefined,
})

const getters: GetterTree<State, RootState> = {
    coords: (state: State) => state.history[state.index] ?? [],
    canUndo: (state: State) => state.index >= 0,
    canRedo: (state: State) => (state.index === undefined && state.history.length > 0) || state.index < state.history.length - 1,
}

const actions: ActionTree<State, RootState> = {
    async addCoord(this: Store<RootState>, { commit }: ActionContext<State, RootState>, coord: Coord) {
        commit('addCoord', coord)
    },
    async undo(this: Store<RootState>, { commit }: ActionContext<State, RootState>) {
        commit('undo')
    },
    async redo(this: Store<RootState>, { commit }: ActionContext<State, RootState>) {
        commit('redo')
    },
}

const mutations: MutationTree<State> = {
    addCoord (state: State, coord: Coord) {
        const newHistory = state.history.slice(0, (state.index ?? -1) + 1)
        const currentCoord = state.history[state.index] ?? []
        newHistory.push([...currentCoord, coord])
        state.history = newHistory
        state.index = state.history.length - 1
    },
    undo (state: State) {
        if (state.index > 0)
            state.index--
        else
            state.index = undefined
    },
    redo (state: State) {
        if (state.index < state.history.length - 1)
            state.index++
        else
            state.index = 0
    },
}

const module: Module<State, RootState> = {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
}
export default module
